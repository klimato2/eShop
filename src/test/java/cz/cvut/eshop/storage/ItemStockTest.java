package cz.cvut.eshop.storage;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ItemStockTest {

    @Test
    public void increaseItemCount() {
        ItemStock itemStock = new ItemStock(null);
        itemStock.IncreaseItemCount(10);

        assertEquals(10, itemStock.getCount());
    }

    @Test
    public void decreaseItemCount() {
        ItemStock itemStock = new ItemStock(null);
        itemStock.decreaseItemCount(10);

        assertEquals(-10, itemStock.getCount());
    }

}