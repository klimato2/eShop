package cz.cvut.eshop.archive;

import cz.cvut.eshop.shop.Order;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class PurchasesArchiveTest {
    @Test
    public void putOrderToPurchasesArchive_addOrderToArchive_shouldAddsOrderToOrderArchiveAndToPurchaseOrderArchive() {
        StandardItem item = new StandardItem(1, "pen", 10, "writing accessories", 1);
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(item);
        Order order = new Order(cart);

        PurchasesArchive archive = new PurchasesArchive();
        archive.putOrderToPurchasesArchive(order);

        List<Order> list = new ArrayList();
        list.add(order);
        Map<Integer, ItemPurchaseArchiveEntry> map = new HashMap();
        map.put(item.getID(), new ItemPurchaseArchiveEntry(item));

        assertEquals(list, archive.getOrderArchive());
        assertEquals(map.size(), archive.getItemPurchaseArchive().size());
        assertEquals(map.get(item.getID()).toString(), archive.getItemPurchaseArchive().get(item.getID()).toString());
    }

    @Test
    public void putOrderToPurchasesArchive_addSameItemToOrderTwice_shouldAddItToSameHashmap() {
        StandardItem item = new StandardItem(1, "pen", 10, "writing accessories", 1);
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(item);
        cart.addItem(item);
        Order order = new Order(cart);

        PurchasesArchive archive = new PurchasesArchive();
        archive.putOrderToPurchasesArchive(order);

        ArrayList<Order> list = new ArrayList();
        list.add(order);
        HashMap<Integer, ItemPurchaseArchiveEntry> map = new HashMap();
        ItemPurchaseArchiveEntry purchaseArchiveEntry = new ItemPurchaseArchiveEntry(item);
        purchaseArchiveEntry.increaseCountHowManyTimesHasBeenSold(1);
        map.put(item.getID(), purchaseArchiveEntry);

        assertEquals(list, archive.getOrderArchive());
        assertEquals(map.size(), archive.getItemPurchaseArchive().size());
        assertEquals(map.get(item.getID()).toString(), archive.getItemPurchaseArchive().get(item.getID()).toString());
    }

    @Test
    public void getHowManyTimesHasBeenItemSold_addSameItemToTwoCarts_shouldReturnItemHasBeenSold2Times() {
        StandardItem item1 = new StandardItem(1, "pen", 10, "writing accessories", 1);
        StandardItem item2 = new StandardItem(2, "pencil", 5, "writing accessories", 0);

        ShoppingCart cart1 = new ShoppingCart();
        ShoppingCart cart2 = new ShoppingCart();
        cart1.addItem(item1);
        cart2.addItem(item1);
        cart2.addItem(item2);
        Order order1 = new Order(cart1);
        Order order2 = new Order(cart2);

        PurchasesArchive archive = new PurchasesArchive();
        archive.putOrderToPurchasesArchive(order1);
        archive.putOrderToPurchasesArchive(order2);

        assertEquals(2, archive.getHowManyTimesHasBeenItemSold(item1));
    }
}
