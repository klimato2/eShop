package cz.cvut.eshop.archive;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;

import static org.junit.Assert.*;

public class ItemPurchaseArchiveEntryTest {
    @Test
    public void increaseCountHowManyTimesHasBeenSold_increaseSoldCounterBy2_shouldReturn3() {
        Item item = new StandardItem(1, "pen", 10, "writing accessories", 1);
        ItemPurchaseArchiveEntry itemArchive = new ItemPurchaseArchiveEntry(item);
        itemArchive.increaseCountHowManyTimesHasBeenSold(2);

        assertEquals(3, itemArchive.getCountHowManyTimesHasBeenSold());
    }

    @Test
    public void getRefItem_gettingThisItemFromArchive_shouldReturnCorrectItem() {
        Item item = new StandardItem(1, "pen", 10, "writing accessories", 1);
        ItemPurchaseArchiveEntry itemArchive = new ItemPurchaseArchiveEntry(item);
        Item correctItem = itemArchive.getRefItem();

        assertEquals(item, correctItem);
    }

    @Test
    public void toString_callThisItemToString_shouldReturnCorrectText() {
        Item item = new StandardItem(1, "pen", 10, "writing accessories", 1);
        ItemPurchaseArchiveEntry itemArchive = new ItemPurchaseArchiveEntry(item);

        assertEquals("ITEM  "+item.toString()+"   HAS BEEN SOLD "+1+" TIMES", itemArchive.toString());
    }
}